package ru.t1.akolobov.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.akolobov.tm.dto.model.SessionDto;

@Repository
@Scope("prototype")
public interface SessionDtoRepository extends UserOwnedDtoRepository<SessionDto> {

}
