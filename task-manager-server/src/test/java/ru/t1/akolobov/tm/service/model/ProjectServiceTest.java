package ru.t1.akolobov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import ru.t1.akolobov.tm.api.service.model.IProjectService;
import ru.t1.akolobov.tm.data.model.TestProject;
import ru.t1.akolobov.tm.enumerated.SortBy;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.NameEmptyException;
import ru.t1.akolobov.tm.exception.field.StatusEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.repository.model.ProjectRepository;
import ru.t1.akolobov.tm.repository.model.UserRepository;

import java.util.ArrayList;
import java.util.List;

import static ru.t1.akolobov.tm.data.model.TestProject.createProject;
import static ru.t1.akolobov.tm.data.model.TestProject.createProjectList;
import static ru.t1.akolobov.tm.data.model.TestUser.*;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    @Autowired
    private static ProjectRepository repository;

    @Autowired
    private static UserRepository userRepository;

    @Autowired
    private IProjectService service;

    @BeforeClass
    public static void addUsers() {
        userRepository.save(USER1);
        userRepository.save(USER2);
    }

    @AfterClass
    public static void deleteUsers() {
        userRepository.delete(USER1);
        userRepository.delete(USER2);
    }

    @Before
    public void initRepository() {
        TestProject.createProjectList(USER1).forEach(repository::save);
    }

    @After
    public void clearRepository() {
        repository.deleteByUserId(USER1_ID);
        repository.deleteByUserId(USER2_ID);
    }

    @Test
    public void add() {
        @NotNull final Project project = createProject(USER1);
        service.add(USER1_ID, project);
        Assert.assertEquals(
                project,
                repository.findByUserIdAndId(
                        project.getUser().getId(),
                        project.getId()
                ).orElse(null)
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(USER_EMPTY_ID, project));
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(USER_EMPTY_ID));
        @NotNull final List<Project> projectList = createProjectList(USER2);
        service.add(projectList);
        Assert.assertFalse(service.findAll(USER2_ID).isEmpty());
        service.clear(USER2_ID);
        Assert.assertTrue(service.findAll(USER2_ID).isEmpty());
    }

    @Test
    public void existById() {
        @NotNull final Project project = createProject(USER1);
        service.add(USER1_ID, project);
        Assert.assertTrue(service.existById(USER1_ID, project.getId()));
        Assert.assertFalse(service.existById(USER2_ID, project.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existById(USER1_ID, USER_EMPTY_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existById(USER_EMPTY_ID, project.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final List<Project> projectList = createProjectList(USER2);
        service.add(projectList);
        Assert.assertEquals(projectList, service.findAll(USER2_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(USER_EMPTY_ID));
    }


    @Test
    public void findAllSorted() {
        @NotNull final Project project = createProject(USER1);
        project.setName("project-0");
        project.setDescription("project-0-desc");
        @NotNull final List<Project> projectList = new ArrayList<>();
        projectList.add(project);
        projectList.addAll(service.findAll(USER1_ID));
        service.add(USER1_ID, project);
        @NotNull final List<Project> projectList2 = service.findAll(
                USER1_ID, Sort.by(SortBy.BY_NAME.getColumnName())
        );
        Assert.assertEquals(projectList, projectList2);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findAll(USER_EMPTY_ID, Sort.by(SortBy.BY_NAME.getColumnName()))
        );
    }

    @Test
    public void findOneById() {
        @NotNull final Project project = createProject(USER1);
        service.add(USER1_ID, project);
        Assert.assertEquals(project, service.findOneById(USER1_ID, project.getId()).orElse(null));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findOneById(USER_EMPTY_ID, project.getId())
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.findOneById(USER1_ID, USER_EMPTY_ID)
        );
    }

    @Test
    public void getSize() {
        int size = service.findAll(USER1_ID).size();
        Assert.assertEquals(size, service.getSize(USER1_ID).intValue());
        service.add(USER1_ID, createProject(USER1));
        Assert.assertEquals(size + 1, service.getSize(USER1_ID).intValue());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(USER_EMPTY_ID));
    }

    @Test
    public void remove() {
        @NotNull final List<Project> projectList = service.findAll(USER1_ID);
        int size = projectList.size();
        @NotNull final Project project = projectList.get(size - 1);
        Assert.assertNotNull(project);
        service.remove(USER1_ID, project);
        Assert.assertFalse(service.findAll(USER1_ID).contains(project));
        Assert.assertNull(service.findOneById(USER1_ID, project.getId()).orElse(null));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(USER_EMPTY_ID, project));
    }

    @Test
    public void removeById() {
        @NotNull final List<Project> projectList = service.findAll(USER1_ID);
        int size = projectList.size();
        @NotNull final Project project = projectList.get(size - 1);
        Assert.assertNotNull(project);
        service.removeById(USER1_ID, project.getId());
        Assert.assertFalse(service.findAll(USER1_ID).contains(project));
        Assert.assertNull(service.findOneById(USER1_ID, project.getId()).orElse(null));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(USER_EMPTY_ID, project.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(USER1_ID, USER_EMPTY_ID));
    }

    @Test
    public void changeStatusById() {
        Project project = createProject(USER1);
        service.add(USER1_ID, project);
        String newProjectId = project.getId();
        Assert.assertNotNull(service.changeStatusById(USER1_ID, newProjectId, Status.IN_PROGRESS));
        project = service.findOneById(USER1_ID, newProjectId).orElse(null);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.changeStatusById(USER_EMPTY_ID, newProjectId, Status.IN_PROGRESS)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.changeStatusById(USER1_ID, USER_EMPTY_ID, Status.IN_PROGRESS)
        );
        Assert.assertThrows(
                StatusEmptyException.class,
                () -> service.changeStatusById(USER1_ID, newProjectId, null)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> service.changeStatusById(USER2_ID, newProjectId, Status.IN_PROGRESS)
        );
    }

    @Test
    public void create() {
        @NotNull final Project project = createProject(USER1);
        Project newProject = service.create(USER1_ID, project.getName());
        Assert.assertNotNull(newProject);
        newProject = service.findOneById(USER1_ID, newProject.getId()).orElse(null);
        Assert.assertNotNull(newProject);
        Assert.assertEquals(project.getName(), newProject.getName());

        newProject = service.create(USER1_ID, project.getName(), project.getDescription());
        Assert.assertNotNull(newProject);
        newProject = service.findOneById(USER1_ID, newProject.getId()).orElse(null);
        Assert.assertNotNull(newProject);
        Assert.assertEquals(project.getName(), newProject.getName());
        Assert.assertEquals(project.getDescription(), newProject.getDescription());

        project.setStatus(Status.IN_PROGRESS);
        newProject = service.create(USER1_ID, project.getName(), project.getStatus());
        Assert.assertNotNull(newProject);
        newProject = service.findOneById(USER1_ID, newProject.getId()).orElse(null);
        Assert.assertNotNull(newProject);
        Assert.assertEquals(project.getName(), newProject.getName());
        Assert.assertEquals(project.getStatus(), newProject.getStatus());

        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(USER_EMPTY_ID, project.getName()));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(USER1_ID, ""));
    }

    @Test
    public void updateById() {
        @NotNull final Project project = service.findAll(USER1_ID).get(0);
        Assert.assertNotNull(project);
        @NotNull final String newName = "NewName";
        @NotNull final String newDescription = "NewDescription";
        service.updateById(USER1_ID, project.getId(), newName, newDescription);
        final Project newProject = service.findOneById(USER1_ID, project.getId()).orElse(null);
        Assert.assertNotNull(newProject);
        Assert.assertEquals(newName, newProject.getName());
        Assert.assertEquals(newDescription, newProject.getDescription());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateById(USER_EMPTY_ID, project.getId(), newName, newDescription)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.updateById(USER1_ID, USER_EMPTY_ID, newName, newDescription)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateById(USER1_ID, project.getId(), "", newDescription)
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> service.updateById(USER2_ID, project.getId(), newName, newDescription)
        );
    }

}
