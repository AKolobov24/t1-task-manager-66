package ru.t1.akolobov.tm.endpoint;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.akolobov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.akolobov.tm.dto.request.ApplicationSystemInfoRequest;
import ru.t1.akolobov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.akolobov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.akolobov.tm.dto.response.ApplicationSystemInfoResponse;
import ru.t1.akolobov.tm.dto.response.ApplicationVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.akolobov.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Autowired
    private final IPropertyService propertyService;

    @NotNull
    @Override
    @WebMethod
    public ApplicationAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ApplicationAboutRequest request
    ) {
        @NotNull final ApplicationAboutResponse response = new ApplicationAboutResponse();
        response.setName(propertyService.getApplicationAuthor());
        response.setEmail(propertyService.getAuthorEmail());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ApplicationVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ApplicationVersionRequest request
    ) {
        @NotNull final ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

    @NotNull
    @Override
    public ApplicationSystemInfoResponse getSystemInfo(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ApplicationSystemInfoRequest request
    ) {
        @NotNull final ApplicationSystemInfoResponse response = new ApplicationSystemInfoResponse();
        response.setHostName(propertyService.getServerHost());
        response.setProcessors(Runtime.getRuntime().availableProcessors());
        response.setFreeMemory(Runtime.getRuntime().freeMemory());
        response.setMaxMemory(Runtime.getRuntime().maxMemory());
        response.setTotalMemory(Runtime.getRuntime().totalMemory());
        return response;
    }

}
