package ru.t1.akolobov.tm.web.configuration;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.t1.akolobov.tm.web.api.service.IDatabaseProperty;

import javax.sql.DataSource;
import java.util.Properties;

@EnableTransactionManagement
@ComponentScan("ru.t1.akolobov.tm.web")
@EnableJpaRepositories("ru.t1.akolobov.tm.web.repository")
public class ApplicationConfiguration {

    @Autowired
    private IDatabaseProperty databaseProperty;

    @Bean
    @NotNull
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseProperty.getDatabaseDriver());
        dataSource.setUrl(databaseProperty.getDatabaseUrl());
        dataSource.setUsername(databaseProperty.getDatabaseUser());
        dataSource.setPassword(databaseProperty.getDatabasePassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.akolobov.tm.web.model");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, databaseProperty.getDatabaseDialect());
        properties.put(Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseDDLAuto());
        properties.put(Environment.SHOW_SQL, databaseProperty.getDatabaseShowSQL());
        if (Boolean.parseBoolean(databaseProperty.getSecondLvlCache())) {
            properties.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getSecondLvlCache());
            properties.put(Environment.CACHE_REGION_FACTORY, databaseProperty.getFactoryClass());
            properties.put(Environment.USE_QUERY_CACHE, databaseProperty.getUseQueryCache());
            properties.put(Environment.USE_MINIMAL_PUTS, databaseProperty.getUseMinimalPuts());
            properties.put(Environment.CACHE_REGION_PREFIX, databaseProperty.getRegionPrefix());
            properties.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getConfigFilePath());
        }
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

}
