package ru.t1.akolobov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.akolobov.tm.api.endpoint.IConnectionProvider;
import ru.t1.akolobov.tm.api.service.IPropertyService;

@Getter
@Setter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService, IConnectionProvider {

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    @Value("${password.secret}")
    private String passwordSecret;

    @NotNull
    @Value("${password.iteration:3123}")
    private Integer passwordIteration;

    @NotNull
    @Value("${server.port:6060}")
    private String serverPort;

    @NotNull
    @Value("${server.host:0.0.0.0}")
    private String serverHost;

    @NotNull
    @Value("${command.folder:./}")
    private String commandFolder;

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getApplicationAuthor() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getEmptyValue() {
        return EMPTY_VALUE;
    }

}
