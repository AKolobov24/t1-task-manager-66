package ru.t1.akolobov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.TaskDto;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskGetByProjectIdResponse extends AbstractResponse {

    @NotNull
    private List<TaskDto> taskList = new ArrayList<>();

    public TaskGetByProjectIdResponse(@NotNull final List<TaskDto> taskList) {
        this.taskList = taskList;
    }

}
