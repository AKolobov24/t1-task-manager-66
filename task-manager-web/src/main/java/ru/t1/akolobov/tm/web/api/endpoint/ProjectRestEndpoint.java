package ru.t1.akolobov.tm.web.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.web.model.Project;

import java.util.Collection;

public interface ProjectRestEndpoint {

    @NotNull
    Collection<Project> findAll();

    @Nullable
    Project findById(@NotNull final String id);

    @Nullable
    Project save(@NotNull final Project project);

    void deleteById(@NotNull final String id);

}
