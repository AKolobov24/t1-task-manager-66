package ru.t1.akolobov.tm.service.model;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.akolobov.tm.api.service.model.IProjectTaskService;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.akolobov.tm.exception.field.TaskIdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.model.Task;
import ru.t1.akolobov.tm.repository.model.ProjectRepository;
import ru.t1.akolobov.tm.repository.model.TaskRepository;
import ru.t1.akolobov.tm.repository.model.UserRepository;

import static ru.t1.akolobov.tm.data.model.TestProject.createProjectList;
import static ru.t1.akolobov.tm.data.model.TestTask.createTaskList;
import static ru.t1.akolobov.tm.data.model.TestUser.*;

@Setter
@Category(UnitCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    @Autowired
    private static UserRepository userRepository;

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @Autowired
    private IProjectTaskService service;

    @BeforeClass
    public static void addUsers() {
        userRepository.save(USER1);
        userRepository.save(USER2);
    }

    @AfterClass
    public static void clearUsers() {
        userRepository.delete(USER1);
        userRepository.delete(USER2);
    }

    @Before
    public void initRepository() {
        createTaskList(USER1).forEach(taskRepository::save);
        createProjectList(USER1).forEach(projectRepository::save);
    }

    @After
    public void clearRepository() {
        taskRepository.deleteByUserId(USER1_ID);
        projectRepository.deleteByUserId(USER1_ID);
        taskRepository.deleteByUserId(USER2_ID);
        projectRepository.deleteByUserId(USER2_ID);
    }

    @Test
    public void bindTaskToProject() {
        @NotNull final Project project = projectRepository.findAllByUserId(USER1_ID).get(0);
        @NotNull final Task task = taskRepository.findAllByUserId(USER1_ID).get(0);
        service.bindTaskToProject(USER1_ID, project.getId(), task.getId());
        final Task bindTask = taskRepository.findByUserIdAndId(USER1_ID, task.getId()).orElse(null);
        Assert.assertNotNull(bindTask);
        Assert.assertNotNull(bindTask.getProject());
        Assert.assertEquals(project.getId(), bindTask.getProject().getId());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.bindTaskToProject(USER_EMPTY_ID, project.getId(), task.getId())
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> service.bindTaskToProject(USER1_ID, USER_EMPTY_ID, task.getId())
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> service.bindTaskToProject(USER1_ID, project.getId(), USER_EMPTY_ID)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> service.bindTaskToProject(USER1_ID, project.getId(), project.getId())
        );
        Assert.assertThrows(
                ProjectNotFoundException.class,
                () -> service.bindTaskToProject(USER1_ID, task.getId(), project.getId())
        );
    }

    @Test
    public void unbindTaskFromProject() {
        @NotNull final Project project = projectRepository.findAllByUserId(USER1_ID).get(0);
        @NotNull final Task task = taskRepository.findAllByUserId(USER1_ID).get(0);
        service.bindTaskToProject(USER1_ID, project.getId(), task.getId());
        Task bindTask = taskRepository.findByUserIdAndId(USER1_ID, task.getId()).orElse(null);
        Assert.assertNotNull(bindTask);
        Assert.assertNotNull(bindTask.getProject());
        Assert.assertEquals(project.getId(), bindTask.getProject().getId());

        service.unbindTaskFromProject(USER1_ID, task.getId());
        bindTask = taskRepository.findByUserIdAndId(USER1_ID, task.getId()).orElse(null);
        Assert.assertNotNull(bindTask);
        Assert.assertNull(bindTask.getProject());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.unbindTaskFromProject(USER_EMPTY_ID, task.getId())
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> service.unbindTaskFromProject(USER1_ID, USER_EMPTY_ID)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> service.unbindTaskFromProject(USER1_ID, project.getId())
        );
    }

}
