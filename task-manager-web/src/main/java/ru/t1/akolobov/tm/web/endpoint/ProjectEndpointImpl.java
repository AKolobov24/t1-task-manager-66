package ru.t1.akolobov.tm.web.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.t1.akolobov.tm.web.api.endpoint.ProjectRestEndpoint;
import ru.t1.akolobov.tm.web.api.service.IProjectService;
import ru.t1.akolobov.tm.web.model.Project;

import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
public final class ProjectEndpointImpl implements ProjectRestEndpoint {

    @Autowired
    private IProjectService projectService;

    @NotNull
    @Override
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<Project> findAll() {
        return projectService.findAll();
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public Project findById(@PathVariable("id") @NotNull final String id) {
        return projectService.findById(id);
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public Project save(@RequestBody @NotNull final Project project) {
        return projectService.save(project);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) {
        projectService.deleteById(id);
    }

}
