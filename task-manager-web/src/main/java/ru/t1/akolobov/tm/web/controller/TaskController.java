package ru.t1.akolobov.tm.web.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.akolobov.tm.web.enumerated.Status;
import ru.t1.akolobov.tm.web.model.Task;
import ru.t1.akolobov.tm.web.repository.ProjectRepository;
import ru.t1.akolobov.tm.web.repository.TaskRepository;

@Controller
public final class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @GetMapping("/task/create")
    public String create() {
        @NotNull final Task task = new Task();
        taskRepository.save(task);
        return "redirect:/task/edit?id=" + task.getId();
    }

    @NotNull
    @PostMapping("/task/delete")
    public String delete(
            @RequestParam(name = "id") @NotNull final String id
    ) {
        taskRepository.deleteById(id);
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/edit")
    public ModelAndView edit(
            @RequestParam(name = "id") @NotNull final String id
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        @Nullable final Task task = taskRepository.findById(id).orElse(null);
        if (task == null) {
            modelAndView.setStatus(HttpStatus.valueOf(404));
            return modelAndView;
        }
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", Status.values());
        modelAndView.addObject("projects", projectRepository.findAll());
        return modelAndView;
    }

    @NotNull
    @PostMapping("/task/edit")
    public String edit(
            @ModelAttribute("task") @NotNull final Task task
    ) {
        if (task.getProjectId() != null && task.getProjectId().isEmpty())
            task.setProjectId(null);
        taskRepository.save(task);
        return "redirect:/tasks";
    }

}
