package ru.t1.akolobov.tm.web.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.akolobov.tm.web.model.Project;
import ru.t1.akolobov.tm.web.model.Task;
import ru.t1.akolobov.tm.web.repository.ProjectRepository;
import ru.t1.akolobov.tm.web.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public final class TasksController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @GetMapping("/tasks")
    public ModelAndView index() {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        @NotNull final List<Task> taskList = taskRepository.findAll();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskList);
        modelAndView.addObject(
                "projects",
                projectRepository.findAllById(
                        taskList.stream()
                                .map(Task::getProjectId)
                                .collect(Collectors.toCollection(ArrayList<String>::new))
                ).stream().collect(Collectors.toMap(Project::getId, Project::getName))
        );
        return modelAndView;
    }

}
