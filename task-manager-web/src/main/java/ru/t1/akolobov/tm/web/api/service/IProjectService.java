package ru.t1.akolobov.tm.web.api.service;

import ru.t1.akolobov.tm.web.model.Project;

public interface IProjectService extends IService<Project> {

}
