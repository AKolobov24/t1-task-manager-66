package ru.t1.akolobov.tm.web;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.web.client.ProjectRestClient;
import ru.t1.akolobov.tm.web.marker.IntegrationCategory;
import ru.t1.akolobov.tm.web.model.Project;

import java.util.Arrays;

@Category(IntegrationCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private final ProjectRestClient projectRestClient = ProjectRestClient.client();

    @NotNull
    private final Project project1 = new Project("test-project-1");

    @NotNull
    private final Project project2 = new Project("test-project-2");

    @NotNull
    private final Project project3 = new Project("test-project-3");

    @Before
    public void addData() {
        projectRestClient.save(project1);
        projectRestClient.save(project2);
        projectRestClient.save(project3);
    }

    @After
    public void clearData() {
        if (projectRestClient.findById(project1.getId()) != null)
            projectRestClient.deleteById(project1.getId());
        if (projectRestClient.findById(project2.getId()) != null)
            projectRestClient.deleteById(project2.getId());
        if (projectRestClient.findById(project3.getId()) != null)
            projectRestClient.deleteById(project3.getId());
    }

    @Test
    public void findAll() {
        Assert.assertTrue(
                projectRestClient
                        .findAll()
                        .containsAll(Arrays.asList(project1, project2, project3))
        );
    }

    @Test
    public void findById() {
        Assert.assertEquals(project1, projectRestClient.findById(project1.getId()));
        Assert.assertEquals(project2, projectRestClient.findById(project2.getId()));
        Assert.assertEquals(project3, projectRestClient.findById(project3.getId()));
    }

    @Test
    public void save() {
        @NotNull Project changedProject = new Project();
        @NotNull final Project newProject = new Project("new-project");
        changedProject.setId(project1.getId());
        changedProject.setName("test-project-changed");
        projectRestClient.save(changedProject);
        projectRestClient.save(newProject);
        Assert.assertEquals(changedProject, projectRestClient.findById(project1.getId()));
        Assert.assertEquals(newProject, projectRestClient.findById(newProject.getId()));
        projectRestClient.deleteById(newProject.getId());
    }

    @Test
    public void deleteById() {
        projectRestClient.deleteById(project1.getId());
        Assert.assertNull(projectRestClient.findById(project1.getId()));
        Assert.assertFalse(projectRestClient.findAll().contains(project1));
    }

}
