package ru.t1.akolobov.tm.web.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.akolobov.tm.web.repository.ProjectRepository;

@Controller
public final class ProjectsController {

    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @GetMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("project-list", "projects", projectRepository.findAll());
    }

}
