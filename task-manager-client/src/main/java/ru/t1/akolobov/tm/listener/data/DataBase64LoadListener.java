package ru.t1.akolobov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.request.DataBase64LoadRequest;
import ru.t1.akolobov.tm.event.ConsoleEvent;


@Component
public final class DataBase64LoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-base64";

    @NotNull
    public static final String DESCRIPTION = "Load data from base64 file.";

    @NotNull
    @Override
    public String getEventName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@dataBase64LoadListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA LOAD BASE64]");
        getDomainEndpoint().loadDataBase64(new DataBase64LoadRequest(getToken()));
    }

}
