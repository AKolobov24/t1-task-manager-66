package ru.t1.akolobov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.akolobov.tm.dto.model.UserDto;
import ru.t1.akolobov.tm.dto.request.UserRegistryRequest;
import ru.t1.akolobov.tm.dto.response.UserRegistryResponse;
import ru.t1.akolobov.tm.event.ConsoleEvent;
import ru.t1.akolobov.tm.exception.user.UserNotFoundException;
import ru.t1.akolobov.tm.util.TerminalUtil;

@Component
public final class UserRegistryListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-registry";

    @NotNull
    public static final String DESCRIPTION = "Registry user.";

    @NotNull
    @Override
    public String getEventName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userRegistryListener.getEventName() == #event.name")
    public void handleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @Nullable final String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest();
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        @NotNull final UserRegistryResponse response = getUserEndpoint().registryUser(request);
        @Nullable final UserDto user = response.getUser();
        if (user == null) throw new UserNotFoundException();
        displayUser(user);
    }

}
