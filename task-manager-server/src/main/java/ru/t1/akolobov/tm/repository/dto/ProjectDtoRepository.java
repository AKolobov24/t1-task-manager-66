package ru.t1.akolobov.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.akolobov.tm.dto.model.ProjectDto;

@Repository
@Scope("prototype")
public interface ProjectDtoRepository extends UserOwnedDtoRepository<ProjectDto> {

}
